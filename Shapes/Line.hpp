///////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief The line class and algorithms for working with lines.
/// \author Benno Fünfstück
///////////////////////////////////////////////////////////////////////////////

#ifndef GEOMLIB_SHAPES_LINE_HPP
#define GEOMLIB_SHAPES_LINE_HPP
#include <cstddef>
#include <utility>
#include <algorithm>
#include <limits>
#include "../Vector.hpp"
#include "../Util.hpp"

namespace geomlib {

///////////////////////////////////////////////////////////////////////////////
/// \brief Class for storing the data of a line, ray or line segment.
/// \details This class represents lines, rays or line segments in the form
///          dir * x + pos, where x may have an upper and/or lower limit.
/// \tparam D The dimension of the line.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t D>
class Line {

	public:
		
		explicit Line(Vector<D> const& pos = Vector<D>(), 
			 Vector<D> const& dir = Vector<D>(),
			 float min = -std::numeric_limits<float>::infinity(), 
			 float max = std::numeric_limits<float>::infinity() )
		: m_pos(pos), m_dir(dir), m_min(min), m_max(max) {}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"

		///////////////////////////////////////////////////////////////////////
		/// \brief Returns true if the line has a upper limit for x in the
		///        line equation dir * x + pos.
		/// \return True if the line has a maximum limit, false otherwise.
		///////////////////////////////////////////////////////////////////////
		bool has_max() const {
			return std::numeric_limits<float>::infinity() != m_max;
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Returns true if the line has a lower limit for x in the
		///        line equation dir * x + pos.
		/// \return True if the line has a minimum limit, false otherwise.
		///////////////////////////////////////////////////////////////////////
		bool has_min() const {
			return -std::numeric_limits<float>::infinity() != m_min;
		}

#pragma GCC diagnostic pop

		///////////////////////////////////////////////////////////////////////
		/// \brief Returns the lower bound for x in the line equation dir * x + pos.
		/// \return Lower bound for x if the line has a lower bound, -inf otherwise.
		///////////////////////////////////////////////////////////////////////
		float min() const {
			return m_min;
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Returns the upper bound for x in the line equation dir * x + pos.
		/// \return Upper bound for x if the line has an upper bound, inf otherwise.
		///////////////////////////////////////////////////////////////////////
		float max() const {
			return m_max;
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Set the lower bound of the line.
		/// \param [in] val The lower bound for the line.
		/// \return Reference to the line itself.
		///////////////////////////////////////////////////////////////////////
		Line& min(float val) {
				m_min = val;
				return *this;
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Set the upper bound for the line.
		/// \param [in] val The lower bound for the line.
		/// \return Reference to the line itself.
		///////////////////////////////////////////////////////////////////////
		Line& max(float val) {
				m_max = val;
				return *this;
	    }

		///////////////////////////////////////////////////////////////////////
		/// \brief Unset the lower bound for the line.
		/// \return Reference to *this (the line itself).
		///////////////////////////////////////////////////////////////////////
		Line& unset_min() {
				m_min = -std::numeric_limits<float>::infinity();
				return *this;
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Unset the upper bound for the line.
		/// \return Reference to *this (the line itself).
		///////////////////////////////////////////////////////////////////////
		Line& unset_max() {
				m_max = std::numeric_limits<float>::infinity();
				return *this;
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Get the reference point of the line.
		/// \return The position of the line.
		///////////////////////////////////////////////////////////////////////
		Vector<D> const& pos() const {
				return m_pos;
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Set the position of the line.
		/// \param [in] pos The new position of the line.
		/// \return Reference to the line.
		///////////////////////////////////////////////////////////////////////
		Line& pos(Vector<D> const& pos) {
				m_pos = pos;
				return *this;
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Get the direction vector of the line.
		/// \return The direction vector of the line.
		///////////////////////////////////////////////////////////////////////
		Vector<D> const& dir() const {
				return m_dir;
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Set the direction vector of the line.
		/// \param [in] dir The new direction vector for the line.
		/// \return Reference to the line.
		///////////////////////////////////////////////////////////////////////
		Line& dir(Vector<D> const& dir) {
				m_dir = dir;
				return *this;
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Gets the point for the given x.
		/// \param [in] x The x.
		/// \return Returns dir() * x + pos. The result is undefined if
		///         x is not in range [min(),max()].
		///////////////////////////////////////////////////////////////////////
		Vector<D> point(float x) const {
			DEBUG_ASSERT(min() <= x && x <= max(), "Parameter x out of range for line. ");
			return dir() * x + pos();
		}

	private:
		
		Vector<D> m_pos;
		Vector<D> m_dir;
		float m_min, m_max;
};

///////////////////////////////////////////////////////////////////////////////
/// \brief Checks if a line has the given point.
/// \param [in] line The line.
/// \param [in] vec  The point.
/// \return True if the line has the given point, false otherwise.
//////////////////////////////////////////////////////////////////////////////
template <std::size_t D>
bool has_point(Line<D> const& line, Vector<D> vec) {
	vec-=line.pos();
	float factor = vec[0] / line.dir()[0];
	if(line.has_max() && factor > line.max() ) return false;
	if(line.has_min() && factor < line.min() ) return false;
	return is_collinear(vec, line.dir());
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Computes the nearest point to a given point on a line.
/// \param [in] line The line.
/// \param [in] point The point.
/// \return The nearest point to the given point on the line.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t D>
std::pair<Vector<D>, Vector<D>> nearest(Line<D> const& line, Vector<D> const& point) {
	float x = dot(line.dir(), point - line.pos() ) / square_length(line.dir());
	x=util::clamp(x, line.min(), line.max() );
	return std::make_pair(line.point(x), point);
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Computes the nearest point to a given point on a line. (Arguments flipped)
/// \param [in] point The point.
/// \param [in] line The line.
/// \return The nearest point to the given point on the line. 
///////////////////////////////////////////////////////////////////////////////
template <std::size_t D>
std::pair<Vector<D>, Vector<D>> nearest(Vector<D> const& point, Line<D> const& line) {
	auto res = nearest(line, point);
	return std::make_pair(res.second, res.first);
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Computes the midpoint of a line segment.
/// \return The midpoint of a line segment.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t D>
Vector<D> midpoint(Line<D> const& l) {
  DEBUG_ASSERT(l.has_min() && l.has_max(), "Can only get midpoint of a line segment");
  return l.point(l.min() / 2 + l.max() / 2);
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Computes the nearest points on two lines.
/// \param [in] l1 The first line
/// \param [in] l2 The second line.
/// \warning If the lines are parallel and unlimited, there are infinite points that are "nearest". This function
///          just returns some random point pair.
/// \return A pair<Vector<D>, Vector<D> > that contains the nearest points on the two lines.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t D>
std::pair<Vector<D>, Vector<D>> nearest(Line<D> const& l1, Line<D> const& l2) {
	float d = square_length(l1.dir()) * -square_length(l2.dir()) + dot(l1.dir(), l2.dir() ) * dot(l1.dir(), l2.dir() );
	if(util::isEqual(d,0)) { // Lines are parallel
		std::vector<std::pair<Vector<D>, Vector<D>> > possible_pairs;
		if(!l1.has_min() && !l1.has_max() && !l2.has_min() && !l2.has_max() )
			possible_pairs.push_back(nearest(l1.point(0), l2) );
		if(l1.has_min() ) {
			possible_pairs.push_back(nearest(l1.point(l1.min() ), l2 ) );
		} 
		if(l1.has_max() ) {
			possible_pairs.push_back(nearest(l1.point(l1.max() ), l2) );
		}
		if(l2.has_min() ) {
			possible_pairs.push_back(nearest(l1, l2.point(l2.min() ) ) );
		} 
		if(l2.has_max() ) {
			possible_pairs.push_back(nearest(l1, l2.point(l2.max() ) ) );
		}
		return *std::min_element(possible_pairs.begin(), possible_pairs.end(), 
			[](std::pair<Vector<D>, Vector<D>> const& p1, std::pair<Vector<D>, Vector<D>> const& p2) {
				return square_length(p1.second - p1.first) < square_length(p2.second - p2.first);
			}
		);
	}
	float dx = dot(l1.pos() - l2.pos(), l1.dir() ) * square_length(l2.dir()) - dot(l1.dir(), l2.dir() ) * dot(l1.pos() - l2.pos(), l2.dir() );
	float dy = -square_length(l1.dir()) * dot(l1.pos() - l2.pos(), l2.dir() ) + dot(l1.pos() - l2.pos(), l1.dir() ) * dot(l1.dir(), l2.dir() );
	float x = dx / d;
	float y = dy / d;
	float x_c = util::clamp(x, l1.min(), l1.max());
	float y_c = util::clamp(y, l2.min(), l2.max());
	if(!util::isEqual(x_c,x) ) return nearest(l1.point(x_c), l2);
	if(!util::isEqual(y_c,y) ) return nearest(l1, l2.point(y_c));
	return std::make_pair(l1.point(x_c), l2.point(y_c));
}

using ::length; // Bring in Vector's length function
template <std::size_t D>
float length(Line<D> const& line) {
	if(!line.has_max() || !line.has_min() ) {
		return std::numeric_limits< float >::infinity();
	}
	return length(line.point(line.max() ) - line.point(line.min() ) );
}

using ::square_length; // Don't hide the Vector's square_length function
template <std::size_t D>
float square_length(Line<D> const& line) {
	if(!line.has_max() || !line.has_min() ) {
		return std::numeric_limits< float >::infinity();
	}
	return square_length(line.pos(line.max() ) - line.pos(line.min() ) );
}

template <std::size_t D>
Line<D> make_line_segment(Vector<D> a, Vector<D> b) {
	return Line<D>().dir(b-a).min(0).max(1).pos(a);
}

template <std::size_t D>
Line<D> make_ray(Vector<D> a, Vector<D> direction) {
	return Line<D>().dir(direction).min(0).unset_max().pos(a);
}

} // End of namespace geomlib.

#endif // GEOMLIB_SHAPES_LINE_HPP
