#ifndef GEOMLIB_SHAPES_RECTANGLE_HPP
#define GEOMLIB_SHAPES_RECTANGLE_HPP

#include <utility>
#include <cmath>
#include <boost/range/iterator_range.hpp>
#include "../Algorithms/General.hpp"
#include "../Vector.hpp"
#include "../Util.hpp"
#include "Line.hpp"

namespace geomlib {

///////////////////////////////////////////////////////////////////////////////
/// \brief An axis aligned rectangle.
///////////////////////////////////////////////////////////////////////////////
class AABB {
	
	private:
		
		class GetCorner {
			
			public:
				
				GetCorner(AABB const& aabb) : m_aabb(&aabb) {}
				
				Vector<2> operator()(std::size_t i) const {
					Vector<2> result;
					result = m_aabb->m_position;
					if(i == 1 || i == 2) result += Vector<2>{m_aabb->m_size[0], 0};
					if(i == 2 || i == 3) result += Vector<2>{0, m_aabb->m_size[1]};
					return result;
				}
			
			private:
				
				AABB const* m_aabb;
			
		};
	
	public:
		
		typedef util::ForIterator<GetCorner> CornerIterator;
		typedef Line<2> Edge;
		typedef util::ForIterator<std::function<Edge const(std::size_t)> > EdgeIterator;
		
		AABB() = default;
		AABB(Vector<2> position, Vector<2> size) : m_position(position), m_size(size) {}
		
		void pos(Vector<2> const& vector) { m_position = vector; }
		Vector<2> pos() const { return m_position; }
		
		void size(Vector<2> const& size) { m_size = size; }
		Vector<2> size() const { return m_size; }
		
		boost::iterator_range<CornerIterator> corners() const {
			return boost::iterator_range<CornerIterator>(
				CornerIterator(GetCorner(*this), 0, *this),
				CornerIterator(GetCorner(*this), 4, *this)
			);
		}
		
		boost::iterator_range<EdgeIterator> edges() const {
			auto function = [&](std::size_t i){
				return make_line_segment(corners()[i], corners()[(i+1) % 4]);
			};
			return boost::iterator_range<EdgeIterator>(
				EdgeIterator(function, 0, *this),
				EdgeIterator(function, 4, *this)
			);
		}
		
		
	
	private:
		
		Vector<2> m_position;
		Vector<2> m_size;
	
};

inline bool operator==(AABB const& aabb1, AABB const& aabb2) {
	return aabb1.size() == aabb2.size() && aabb1.pos() == aabb2.pos();
}

inline bool operator!=(AABB const& aabb1, AABB const& aabb2) {
	return !(aabb1 == aabb2);
}

inline bool has_point(AABB const& aabb, Vector<2> const& point) {
	Vector<2> point_rel = point - aabb.pos();
	if(point_rel[0] < 0 && aabb.size()[0] > 0) return false;
	if(point_rel[1] < 0 && aabb.size()[1] > 0) return false;
	return std::abs(point_rel[0]) <= std::abs(aabb.size()[0]) && std::abs(point_rel[1]) <= std::abs(aabb.size()[1]);
}

inline float area(AABB const& aabb) {
	return aabb.size()[0] * aabb.size()[1];
}

inline bool includes(AABB const& aabb1, AABB const& aabb2) {
	return has_point(aabb1, aabb2.pos() ) && has_point(aabb1, aabb2.pos() + aabb2.size());
}

inline std::pair<Vector<2>, Vector<2> > nearest(AABB const& aabb, Vector<2> const& vector) {
	if(has_point(aabb, vector) ) return std::make_pair(vector, vector);
	auto result = std::make_pair(aabb.corners()[0], vector);
	for(AABB::Edge const& edge : aabb.edges() ) {
		auto current = nearest(edge, vector);
		if(square_length(current.second - current.first) < square_length(result.second - result.first) ) {
			result = current;
		}
	}
	return result;
}

inline std::pair<Vector<2>, Vector<2> > nearest(AABB const& aabb1, AABB const& aabb2) {
	if(includes(aabb1, aabb2) ) return std::make_pair(aabb2.corners()[0], aabb2.corners()[0]);
	if(includes(aabb2, aabb1) ) return std::make_pair(aabb1.corners()[0], aabb1.corners()[0]);
	auto const& a = aabb1.edges()[0];
	auto const& b = aabb2.edges()[0];
	std::pair<Vector<2>, Vector<2> > result = nearest(a, b);
	for(AABB::Edge const& edge1 : aabb1.edges() ) {
		for(AABB::Edge const& edge2 : aabb2.edges() ) {
			auto current = nearest(edge1, edge2);
			if(square_length(current.second - current.first) < square_length(result.second - result.first) ) {
				result = current;
			}
		}
	}
	return result;
}

inline std::pair<AABB::Edge, AABB::Edge> nearest_edges(AABB const& aabb1, AABB const& aabb2) {
	auto a = aabb1.edges()[0];
	auto b = aabb2.edges()[0];
	float curmin = minimum_distance(a, b);
	auto result = std::make_pair(a, b);
	for(AABB::Edge const& edge1 : aabb1.edges() ) {
		for(AABB::Edge const& edge2 : aabb2.edges() ) {
			if(minimum_distance(edge1, edge2) < curmin) result = std::make_pair(edge1, edge2);
		}
	}
	return result;
}

inline AABB sweep(AABB const& box, Vector<2> direction) {
  std::array<Vector<2>, 8> points;
  std::copy(box.corners().begin(), box.corners().end(), points.begin() );
  std::transform(box.corners().begin(), box.corners().end(), points.begin() + 4, [&](Vector<2> const& pt) {
    return pt + direction;
  });
  auto compare_x = [](Vector<2> const& vec1, Vector<2> const& vec2) { return vec1[0] < vec2[0]; };
  auto compare_y = [](Vector<2> const& vec1, Vector<2> const& vec2) { return vec1[1] < vec2[1]; };
  auto xlimits = std::minmax_element(points.begin(), points.end(), compare_x);
  auto ylimits = std::minmax_element(points.begin(), points.end(), compare_y);
  return AABB(Vector<2>((*xlimits.first)[0], (*ylimits.first)[1]), Vector<2>((*xlimits.second)[0] - (*xlimits.first)[0], (*ylimits.second)[1] - (*ylimits.first)[1]) );
}
	
} //End namespace geomlib

#endif //GEOMLIB_SHAPES_RECTANGLE_HPP