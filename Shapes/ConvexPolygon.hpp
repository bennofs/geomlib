
///////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief Classes / Datastructures for convex polygons.
/// \author Benno Fünfstück
///////////////////////////////////////////////////////////////////////////////

#ifndef GEOMLIB_SHAPES_CONVEXPOLYGON_HPP
#define GEOMLIB_SHAPES_CONVEXPOLYGON_HPP

#include <iterator>
#include <vector>
#include <functional>
#include <boost/range.hpp>
#include "../Util.hpp"
#include "../Vector.hpp"
#include "../Algorithms/General.hpp"
#include "Line.hpp"

namespace geomlib {

///////////////////////////////////////////////////////////////////////////////
/// \brief Class for a convex polygon.
/// \details This class stores a convex polygon, that is, a polygon whose internal
///          angles are all below or equal to 180 degrees (http://en.wikipedia.org/wiki/Convex_and_concave_polygons)
///////////////////////////////////////////////////////////////////////////////
class ConvexPolygon {

	public:

		typedef std::vector<Vector<2>>::const_iterator PointIterator;
		typedef Line<2> Edge;
		typedef util::ForIterator<std::function<Edge const(std::size_t index)> > EdgeIterator;
		
		///////////////////////////////////////////////////////////////////////
		/// \brief Default constructor.
		///////////////////////////////////////////////////////////////////////
		ConvexPolygon() = default;
		
		///////////////////////////////////////////////////////////////////////
		/// \brief Constructor intializing the points of the convex hull with
		///        points from a std::vector.
		/// \param [in] vec The vector that contains the points.
		///////////////////////////////////////////////////////////////////////
		ConvexPolygon(std::vector<Vector<2> > points) : m_hull(std::move(points) ) { update(); }
		
		///////////////////////////////////////////////////////////////////////
		/// \brief Constructor intializing with a sequence of points.
		/// \param [in] begin Iterator to the begin of the sequence of points,
		/// \param [in] end   Iterator to one past the last element of the sequence of points.
		///////////////////////////////////////////////////////////////////////
		template <typename IIter>
		ConvexPolygon(IIter begin, IIter end) : m_hull(begin, end) { update(); }

		///////////////////////////////////////////////////////////////////////
		/// \brief Extend the convex hull with a given point.
		/// \details This function adds a new point in to convex hull
		///          such that the convex hull stays convex. If the point
		///          is in the convex hull, nothing changes.
		/// \param [in] point The point to add.
		/// \warning Invalidates all iterators of the polygon.
		/// \return The convex polygon itself, to allow chaining of operations.
		///////////////////////////////////////////////////////////////////////
		ConvexPolygon& insert(Vector<2> const& point) {
			m_hull.push_back(point);
			if(m_hull.size() > 2) {
				update();
			}
			return *this;
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Remove a point from the convex polygon.
		/// \param [in] it
		/// \warning Invalidates all iterators of the polygon.
		/// \return The convex polygon itself, to allow chaining of operations.
		///////////////////////////////////////////////////////////////////////
		ConvexPolygon& remove(PointIterator it) {
			m_hull.erase(m_hull.begin() + std::distance(points().begin(), it) );
			if(m_hull.size() > 2) {
				update();
			}
			return *this;
		}


		///////////////////////////////////////////////////////////////////////
		/// \brief Get a view that contains the points of the polygon sorted in counter-clockwise order.
		/// \return PointsView of the polygon.
		//////////////////////////////////////////////////////////////////////
		boost::iterator_range<PointIterator> points() const {
			return boost::make_iterator_range(m_hull.begin(), m_hull.end());
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Get a view of the edges of the polygon.
		/// \return An EdgesView that contains all the edges of the polygon.
		///////////////////////////////////////////////////////////////////////
		boost::iterator_range<EdgeIterator> edges() const {
			auto function = [&](std::size_t i) -> Edge const {
				Vector<2> p1 = m_hull[i];
				Vector<2> p2 = m_hull[(i + 1) % m_hull.size()];
				return Edge().pos(p1).dir(p2-p1).min(0).max(1);
			};
			return util::make_for_iterator_range(function, m_hull);
		}
		
		///////////////////////////////////////////////////////////////
		/// \brief Return the edges of the polygon.
		/// \return The edges of the polygon.
		///////////////////////////////////////////////////////////////
		std::vector<Edge> edges_vec() const {
		  std::vector<Edge> result;
		  std::size_t const si = m_hull.size();
		  result.reserve(si);
		  for(unsigned int i = 0; i < si; ++i) {
		    result.push_back(Edge().pos(m_hull[i]).dir(m_hull[(i + 1) % si] - m_hull[i]).min(0).max(1));
		  }
		  return result;
		}
		

	private:

		///////////////////////////////////////////////////////////////////////
		/// \brief Update the convex hull.
		/// \details This function reorders the points in the convex hull and removes
		///          points that are not on the boundary of the convex hull. It uses
		///          the Monotone Chain Algorithm (
		///          http://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain)
		///////////////////////////////////////////////////////////////////////
		void update();

		std::vector<Vector<2> > m_hull;

};

///////////////////////////////////////////////////////////////////////////////
/// \brief Checks whether the given point is in a given convex polygon.
/// \details This function tests if the point is on the same side of each edge of the polygon, 
///          which means that it is either in- or outside the convex polygon.
/// \param [in] poly The polygon to check.
/// \param [in] vec  The point to check for.
///////////////////////////////////////////////////////////////////////////////
inline bool has_point(ConvexPolygon const& poly, Vector<2> const& vec) {
	auto check_one = [&](ConvexPolygon::Edge const& edge) { return is_clockwise({edge.point(0), edge.point(1), vec}); };
	std::vector<ConvexPolygon::Edge> edges = poly.edges_vec();
	bool all = true, none = true;
	for(ConvexPolygon::Edge const& e : edges) {
	  bool res = check_one(e);
	  all = all && res;
	  none = none && !res;
	}
	return all || none;
}


inline std::pair<Vector<2>, Vector<2>> nearest(ConvexPolygon polygon, Line<2> line) {
	std::pair<Vector<2>, Vector<2> > result;
	for(ConvexPolygon::Edge const& edge : polygon.edges() ) {
		std::pair<Vector<2>, Vector<2>> current = nearest(Line<2>(edge), Line<2>(line) );
		if(square_length(current.second - current.first) < square_length(result.second - result.first) ) {
			result = current;
		}
	}
	return result;
}


inline void geomlib::ConvexPolygon::update() {
	if(m_hull.size() < 3) return;
	std::sort(m_hull.begin(), m_hull.end(), [](Vector<2> const& a, Vector<2> const&b) {
		return a[0] < b[0] || (util::isEqual(a[0],b[0]) && a[1] < b[1]);
	});
	std::vector<Vector<2> > new_hull;
	new_hull.reserve(m_hull.size() );
	for(std::size_t i = 0; i < m_hull.size(); ++i) {
		new_hull.push_back(m_hull[i]);
		while(new_hull.size() >= 3 && is_clockwise(new_hull.end() - 3, new_hull.end() ) ) {
			std::swap(new_hull.back(), *(new_hull.end() - 2) );
			new_hull.pop_back();
		}
	}
	new_hull.pop_back(); // Same point as first of upper bound
	std::size_t lower_hull_size = new_hull.size();
	for(std::size_t i = m_hull.size() - 1; i < m_hull.size(); i--) {
		new_hull.push_back(m_hull[i]);
		while(new_hull.size() >= lower_hull_size + 3 && is_clockwise(new_hull.end() - 3, new_hull.end() ) ) {
			std::swap(new_hull.back(), *(new_hull.end() -2) );
			new_hull.pop_back();
		}
	}
	new_hull.pop_back();
	m_hull = std::move(new_hull);
}


} //End of geomlib namespace

#endif // GEOMLIB_SHAPES_CONVEXPOLYGON_HPP