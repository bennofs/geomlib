///////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief Contains the triangle class and algorithms for working with triangles.
/// \author Benno Fünfstück
///////////////////////////////////////////////////////////////////////////////

#ifndef GEOMLIB_SHAPES_TRIANGLE
#define GEOMLIB_SHAPES_TRIANGLE

#include <cstddef>
#include <array>
#include "../Vector.hpp"
#include "../Util.hpp"
#include "Line.hpp"
#include "Circle.hpp"

namespace geomlib {

namespace detail_ {
	class Triangle_Type_ID_ {};
}

///////////////////////////////////////////////////////////////////////////////
/// \brief A class for triangles.
/// \tparam D The dimension of the triangle.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t D>
class Triangle : public util::StrongType<std::array<Vector<D>, 3>, detail_::Triangle_Type_ID_> {

	public:
		
		Triangle() = default;
		
		Triangle(Vector<D> const& a_, Vector<D> const& b_, Vector<D> const& c_) {
			A(a_); 
			B(b_); 
			C(c_);
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Get the first point of the triangle.
		/// \return The first point of the triangle.
		///////////////////////////////////////////////////////////////////////
		Vector<D> const& A() const {
				return (*this)[0];
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Get the second point of the triangle.
		/// \return The second point of the triangle.
		///////////////////////////////////////////////////////////////////////
		Vector<D> const& B() const {
				return (*this)[1];
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Get the third point of the triangle.
		/// \return The third point of the triangle.
		///////////////////////////////////////////////////////////////////////
		Vector<D> const& C() const {
				return (*this)[2];
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Set the first point of the triangle.
		/// \param [in] pos The position for the first point.
		/// \return Reference to the triangle.
		///////////////////////////////////////////////////////////////////////
		Triangle& A(Vector<D> const& pos) {
				(*this)[0] = pos;
				return *this;
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Set the second point of the triangle.
		/// \param [in] pos The position for the second point.
		/// \return Reference to the triangle.
		///////////////////////////////////////////////////////////////////////
		Triangle& B(Vector<D> const& pos) {
				(*this)[1] = pos;
				return *this;
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Set the third point of the triangle.
		/// \param [in] pos The position of the third point.
		/// \return Reference to the triangle.
		///////////////////////////////////////////////////////////////////////
		Triangle& C(Vector<D> const& pos) {
				(*this)[2] = pos;
				return *this;
		}
		
		///////////////////////////////////////////////////////////////////////
		/// \brief Get the edge on the opposite of the point A.
		/// \return The edge a.
		///////////////////////////////////////////////////////////////////////
		Line<D> a() const {
		  return make_line_segment(B(), C());
		}
		  
		///////////////////////////////////////////////////////////////////////
		/// \brief Get the edge on the opposite of the point B.
		/// \return The edge b.
		///////////////////////////////////////////////////////////////////////
		Line<D> b() const {
		  return make_line_segment(A(), C());
		}
		
		///////////////////////////////////////////////////////////////////////
		/// \brief Get the edge on the opposite of the point C.
		///////////////////////////////////////////////////////////////////////
		Line<D> c() const {
		  return make_line_segment(A(), B());
		}
		
		///////////////////////////////////////////////////////////////////////
		/// \brief Get the point at given Barycentric Coordinates.
		/// \param [in] x The first coordinate.
		/// \param [in] y The second coordinate.
		///////////////////////////////////////////////////////////////////////
		Vector<D> point(float x, float y) {
			return A() + x * (C() - A()) + y * (B() - A());
		}

};

///////////////////////////////////////////////////////////////////////////////
/// \brief Checks if a point is in the given triangle.
/// \details This function uses Barycentric Cordinates to check if a point is in
///          the given triangle. See http://www.blackpawn.com/texts/pointinpoly/default.html
///          for more details.
/// \param [in] triangle The triangle.
/// \param [in] vec      The point.
/// \return True if the given point is in the triangle, false otherwise.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t D>
bool has_point(Triangle<D> const& triangle, Vector<D> const& vec) {
	// Compute vectors.
	Vector<D> v0, v1, v2;
	v0 = triangle.C() - triangle.A();
	v1 = triangle.B() - triangle.A();
	v2 = vec - triangle.A();
	// Compute dot products.
	float dot00 = dot(v0, v0),
	      dot01 = dot(v0, v1),
	      dot02 = dot(v0, v2),
	      dot11 = dot(v1, v1),
	      dot12 = dot(v1, v2);
	// Compute barycentric coordinates.
	float invDenorm = 1 / (dot00 * dot11 - dot01 * dot01);
	float u = (dot11 * dot02 - dot01 * dot12) * invDenorm;
	float v = (dot00 * dot12 - dot01 * dot02) * invDenorm;
	// Check if point is in triangle
	return (u>=0) && (v>=0) && (u+v) <= 1;
}

template <std::size_t D>
float area(Triangle<D> const& triangle) {
	auto vecA = triangle.B() - triangle.A();
	auto vecB = triangle.C() - triangle.A();
	float a = length(vecA);
	float A_to_h_start = dot(vecA, vecB) / a;
	float h = std::sqrt(A_to_h_start * A_to_h_start + square_length(vecB) );
	return h * a * 0.5f;
}

Circle<2> circumscribed_circle(Triangle<2> const& triangle) {
  Line<2> ha = Line<2>(midpoint(triangle.a()), {-triangle.a().dir()[1], triangle.a().dir()[0]});
  Line<2> hb = Line<2>(midpoint(triangle.b()), {-triangle.b().dir()[1], triangle.b().dir()[0]});
  Vector<2> c1,c2;
  std::tie(c1,c2) = nearest(ha,hb);
  return Circle<2>(c1,length(triangle.A() - c1));
}

} //End of namespace geomlib

#endif // GEOMLIB_SHAPES_TRIANGLE
