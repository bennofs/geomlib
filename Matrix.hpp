///////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief Operations on matrices.
/// \author Benno Fünfstück
///////////////////////////////////////////////////////////////////////////////

#include <array>
#include <algorithm>
#include <ostream>
#include <type_traits>
#include <functional>
#include "Util.hpp"
#include "Vector.hpp"

namespace geomlib {

namespace detail_  {
	struct Matrix_Type_ID_ {};
}

template <std::size_t M, std::size_t N, typename T=float>
using Matrix = util::StrongType<util::Array<Vector<N, T>, M>, detail_::Matrix_Type_ID_>;

///////////////////////////////////////////////////////////////////////////////
/// \brief Returns the indentity matrix.
/// \return A matrix A such that AX = X and XA = X
///////////////////////////////////////////////////////////////////////////////
template <std::size_t M, typename T = float>
Matrix<M, M, T> IdentityMatrix() {
	Matrix<M, M, T> result;
	for(std::size_t i = 0; i < M; ++i) {
		for(std::size_t j = 0; j < M; ++j) {
			result[i][j] = (i == j) ? 1 : 0;
		}
	}
	return result;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Returns a null matrix.
/// \return A matrix with all elements set to 0.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t M, std::size_t N, typename T = float>
Matrix<M, N, T> NullMatrix() {
	Matrix<M, N, T> result;
	result.fill(NullVector<N, T>());
	return result;
}

} // End of namespace geomlib

///////////////////////////////////////////////////////////////////////////////
/// \brief Calculates the sum of two matrices.
/// \details The sum of two matrices is calculated by adding the corresponding
///          elements.
/// \param [in] mat1 The first matrix.
/// \param [in] mat2 The second matrix.
/// \return A new matrix that is the sum of mat1 and mat2.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t M, std::size_t N, typename T1, typename T2>
geomlib::Matrix<M, N, typename std::common_type<T1, T2>::type> operator+(geomlib::Matrix<M, N, T1> const& mat1, geomlib::Matrix<M, N, T2> const& mat2) {
	geomlib::Matrix<M, N, typename std::common_type<T1,T2>::type> result;
	typedef geomlib::Vector<N,typename std::common_type<T1,T2>::type> vec;
	std::transform(
		mat1.begin(),
		mat1.end(),
		mat2.begin(),
		result.begin(),
		[](vec const& vec1, vec const& vec2) { return vec1 + vec2; }
	);
	return result;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Calculates the difference of two matrices.
/// \details The difference of two matrices is calculated by subtracting
///          the corresponding elements.
/// \param [in] mat1 The first matrix.
/// \param [in] mat2 The second matrix.
/// \return The difference of mat1 and mat2.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t M, std::size_t N, typename T1, typename T2>
geomlib::Matrix<M, N, typename std::common_type<T1, T2>::type> operator-(geomlib::Matrix<M, N, T1> const& mat1, geomlib::Matrix<M, N, T2> const& mat2) {
	geomlib::Matrix<M, N, typename std::common_type<T1,T2>::type> result;
	std::transform(
		mat1.begin(),
		mat1.end(),
		mat2.begin(),
		result.begin(),
		std::minus<geomlib::Vector<N,typename std::common_type<T1,T2>::type>>()
	);
	return result;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Calculates the product of a matrix and a scalar.
/// \param [in] mat The matrix.
/// \param [in] val The scalar.
/// \return The new matrix containing the elements of the given matrix multiplied
///         by the scalar.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t M, std::size_t N, typename T1, typename T2>
geomlib::Matrix<M, N, typename std::common_type<T1, T2>::type> operator*(geomlib::Matrix<M, N, T1> const& mat1, T2 const& val) {
	geomlib::Matrix<M, N, typename std::common_type<T1,T2>::type> result;
	typedef geomlib::Vector<N,typename std::common_type<T1,T2>::type> vec;
	auto it = result.begin();
	for(auto const& line1 : mat1) {
			*it++ = line1 * val;
	}
	return result;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Calculates the product of two matrices.
/// \param [in] mat1 The first matrix.
/// \param [in] mat2 The second matrix.
/// \return The matrix product mat1 * mat2 of the two matrices.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t M, std::size_t N, std::size_t O, typename T1, typename T2>
geomlib::Matrix<M, O, typename std::common_type<T1, T2>::type> operator*(geomlib::Matrix<M, N, T1> const& mat1, geomlib::Matrix<N, O, T2> const& mat2) {
	geomlib::Matrix<M, O, typename std::common_type<T1, T2>::type> result;
	auto line_out = result.begin();
	auto out = line_out->begin();
	for(auto const& line1 : mat1) {
		for(auto const& column2 : mat2) {
			*out++ = dot(line1, column2);
		}
		out = (++line_out)->begin();
	}
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Checks whether two matrices are identical or not.
/// \param [in] mat1 The first matrix.
/// \param [in] mat2 The second matrix.
/// \return True if the two matrices are equal, false otherwise.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t M, std::size_t N, typename T1, typename T2>
bool operator==(geomlib::Matrix<M,N,T1> const& mat1, geomlib::Matrix<M,N,T2> const& mat2) {
	return std::equal(mat1.begin(), mat1.end(), mat2.begin());
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Print a matrix to a stream.
/// \param [out] stream The stream to which to write.
/// \param [in]  matrix The matrix which to write.
/// \return The stream.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t M, std::size_t N, typename T>
std::ostream& operator<<(std::ostream& stream, geomlib::Matrix<M, N, T> const& matrix) {
	for(auto const& line : matrix) {
		stream << line << "\n";
	}
	return stream;
}