#include "catch.hpp"
#include "../Vector.hpp"
#include "../Util.hpp"

using namespace geomlib;

TEST_CASE("GeomLib/Vector/Intialization", "Test the intialization of the vector") {
	SECTION("Intialization using an intializer_list", "Test the initialization from an intializer_list") {
		Vector<5,int> intVec{-1,-3,2,4,2};
		REQUIRE(intVec[0] == -1);
		REQUIRE(intVec[1] == -3);
		REQUIRE(intVec[2] == 2);
		REQUIRE(intVec[3] == 4);
		REQUIRE(intVec[4] == 2);
		Vector<2,float> floatVec{-1.1, 2.1};
		REQUIRE(util::isEqual(floatVec[0],-1.1));
		REQUIRE(util::isEqual(floatVec[1],2.1));
	}
	SECTION("Copy intialization", "Test the intialization from another vector") {
		Vector<5,int> intVec{-1,-3,2,4,2};
		Vector<5,int> c(intVec);
		REQUIRE(intVec == c);

	}
}

TEST_CASE("GeomLib/Vector/Comparision", "Test the comparision operators") {
	Vector<3,float> v1 = {-1.1f,-4,1};
	REQUIRE((v1 == Vector<3,float>({-1.1f,-4,1})));
}

TEST_CASE("GeomLib/Vector/Unary operators", "Test the unary operators (-,+)") {
	Vector<3,int> vec = {-1,-2,3};
	REQUIRE(((-vec) == Vector<3,int>({1,2,-3})));
	REQUIRE(vec == +(vec) );
 	REQUIRE( (+vec == -(-vec)) );
}

TEST_CASE("GeomLib/Vector/Binary operators", "Test the binary operators (-,+,*,/)") {
	Vector<3,float> v1 = {-1.1f, -2.1f, 9.1f};
	Vector<3,float> v2 = { 1.1f, 3, 0};
	REQUIRE( (v1 + v2 == Vector<3,float>({0,0.9f, 9.1f}) ) );
	REQUIRE( (v1 - v2 == Vector<3,float>({-2.2f, -5.1f, 9.1f}) ) );
	REQUIRE( (v1 * 1) == v1 );
	REQUIRE( (v1 * -1) == -v1);
	REQUIRE( (-2 * v2 == -Vector<3,float>({2.2f, 6.0f, 0.0f}) ) );
	Vector<3,float> v3 = v1 - v2;
	Vector<3,float> v4 = v1 + v2;
	v1-=v2;
	REQUIRE(v1 == v3);
	v1+=v2;
	v1+=v2;
	REQUIRE(v1 == v4);
	v1 /= -2;
	REQUIRE(v1 == -v4 / 2);
	REQUIRE( (v1 * -2) == v4);
	v1 *= 2;
	REQUIRE(v1 == -v4);
}

TEST_CASE("GeomLib/Vector/Operations", "Test other operations on vectors (dot, cross, length, squared length, is_collinear)") {
	Vector<3,float> v1 = {3,-1,2};
	Vector<3,float> v2 = {-5,-10,2};
	REQUIRE(dot(v1,v2) == -1);
	REQUIRE( (cross(v1,v2) == Vector<3,float>({18,-16,-35}) ) );
	REQUIRE( util::isEqual(length(v2), 11.357816691600547) );
	REQUIRE( util::isEqual(length(v1), 3.7416573867739413) );
	REQUIRE( dot(cross(v1, v2), v1) == 0 );
	REQUIRE( dot(cross(v1, v2), v2) == 0 );
	for(std::size_t i = 0; i < 10; ++i) {
		REQUIRE( is_collinear(Vector<2>(3,6), Vector<2>(3,6) * i) );
	}
	REQUIRE_FALSE(is_collinear(Vector<2, int>(2,3), Vector<2, int>(4,7) ) );
	REQUIRE_FALSE(is_collinear(Vector<2,int>(3,3), Vector<2, int>(5,6) ) );
}
