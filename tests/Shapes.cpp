#include <random>
#include <ctime>
#include "catch.hpp"
#include "../Shapes/AABB.hpp"
#include "../Shapes/ConvexPolygon.hpp"
#include "../Shapes/Line.hpp"
#include "../Shapes/Triangle.hpp"
#include "../Util.hpp"

std::mt19937 engine(std::time(NULL) );
using namespace geomlib;

template <std::size_t D>
Vector<D> rand_vector() {
	Vector<D> result;
	std::generate(result.begin(), result.end(), std::bind(std::uniform_real_distribution<float>(), engine));
	return result;
}

TEST_CASE("GeomLib/Shapes/AABB", "Test the AABB shape class and algorithms for AABBs") {
	
	geomlib::AABB aabb({1,0}, {3, 4});
	REQUIRE( (aabb.corners().begin() + 4) == aabb.corners().end() );
	REQUIRE( (aabb.edges().begin() + 4) == aabb.edges().end() );
	REQUIRE(aabb.corners()[0] == Vector<2>(1,0));
	REQUIRE(aabb.corners()[1] == Vector<2>(4,0));
	REQUIRE(aabb.corners()[2] == Vector<2>(4,4));
	REQUIRE(aabb.corners()[3] == Vector<2>(1,4));
	CHECK(has_point(aabb, {1,0}) == true);
	CHECK(has_point(aabb, {4,0}) == true);
	CHECK(has_point(aabb, {4,4}) == true);
	CHECK(area(aabb) == 12);
	for(auto const& point : aabb.corners() ) {
		CHECK(has_point(aabb, point) );
	}
	geomlib::AABB aabb2({5,3}, {4,5});
	std::pair<geomlib::Vector<2>, geomlib::Vector<2> > n = nearest(aabb, aabb2);
	CHECK(length(n.second - n.first) == 1.0);
	CHECK(sweep(aabb, {1, -1}) == AABB(Vector<2>(1, -1), Vector<2>(4, 5)));
}

TEST_CASE("GeomLib/Shapes/Line", "Test the Line class and algorithms for Lines") {
	auto line = geomlib::make_line_segment<2>({1,1}, {3,3});
	auto line2 = geomlib::make_line_segment<2>({1,0}, {3,2});
	REQUIRE(line.point(0) == Vector<2>(1,1));
	REQUIRE(line.point(1) == Vector<2>(3,3));
	REQUIRE(line2.point(0) == Vector<2>(1,0));
	REQUIRE(line2.point(1) == Vector<2>(3,2));
	CHECK(geomlib::util::isEqual(length(line),std::sqrt(8)) );
	auto rand_point_on_line = [&](Line<2> const& l) { 
		return l.point(std::uniform_real_distribution<float>(l.min(), l.max() )(engine) );
	};
	auto rand_point_not_on_line = [&](Line<2> const& l) {
		Vector<2> rand_dir;
		do { rand_dir = rand_vector<2>(); } while(is_collinear(rand_dir, l.dir() ) );
		int factor;
		do { factor = std::uniform_int_distribution<int>(-10, 10)(engine); } while(factor == 0);
		rand_dir *= factor;
		return rand_point_on_line(l) + rand_dir;
		
	};
	for(std::size_t i = 0; i < 100; ++i) {
		CHECK(has_point(line, rand_point_on_line(line)) );
		CHECK(has_point(line2, rand_point_on_line(line2)) );
		CHECK_FALSE(has_point(line, rand_point_not_on_line(line) ) );
		CHECK_FALSE(has_point(line2, rand_point_not_on_line(line2) ) );
	}
	CHECK(has_point(line, line.point(0) ) );
	CHECK(has_point(line, line.point(1) ) );
	CHECK(has_point(line2, line2.point(0) ) );
	CHECK(has_point(line2, line2.point(1) ) );
	CHECK_FALSE(has_point(line, line.pos() + line.dir() * (1.1f) ) );
	CHECK_FALSE(has_point(line, line.pos() + line.dir() * (-0.1f) ) );
	CHECK_FALSE(has_point(line2, line2.pos() + line.dir() * (1.1f) ) );
	CHECK_FALSE(has_point(line2, line2.pos() + line.dir() * (-0.1f) ) );
}

TEST_CASE("GeomLib/Shapes/Triangle", "Test the Triangle class and algorithms for Triangles") {
	Triangle<2> t = {{1,0}, {2,1}, {1.5f, 4}};
	REQUIRE(t.A() == Vector<2>(1,0));
	REQUIRE(t.B() == Vector<2>(2,1));
	REQUIRE(t.C() == Vector<2>(1.5f, 4));
	CHECK(has_point(t, t.A()) );
	CHECK(has_point(t, t.B()) );
	CHECK(has_point(t, t.C()) );
	auto rand_point_on_triangle = [&](Triangle<2> triangle) {
		float a = std::uniform_real_distribution<float>()(engine);
		float b = std::uniform_real_distribution<float>(0, 1-a)(engine);
		return triangle.point(a, b);
	};
	auto rand_point_not_on_triangle = [&](Triangle<2> triangle) {
		float a = std::uniform_real_distribution<float>(1,10)(engine);
		float b = std::uniform_real_distribution<float>(1,10)(engine);
		return triangle.point(a,b);
	};
	for(std::size_t i = 0; i < 100; ++i) {
		CHECK(has_point(t, rand_point_on_triangle(t) ) );
		CHECK_FALSE(has_point(t, rand_point_not_on_triangle(t)) );
	}
	// TODO: Test area function
}

TEST_CASE("GeomLib/Shapes/ConvexPolygon", "Test the ConvexPolygon class and algorithms for convex polygons") {
	
}