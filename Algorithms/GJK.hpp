///////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief Implements the GJK algorithm and functions that belong to it.
///////////////////////////////////////////////////////////////////////////////
#ifndef GEOMLIB_ALGORITHMS_GJK_HPP
#define GEOMLIB_ALGORITHMS_GJK_HPP

#include <cstddef>
#include "../Vector.hpp"
#include "../Shapes/ConvexPolygon.hpp"

namespace geomlib {
namespace gjk {
	
template <typename Object1, typename Object2, std::size_t D>
Vector<D> supporting_point(Object1 const& obj1, Object2 const& obj2, Vector<D> direction) {
	return supporting_point(obj1, direction) - supporting_point(obj2, direction);
}

inline Vector<2> supporting_point(ConvexPolygon const& polygon, Vector<2> direction) {
	return *std::max_element(polygon.points().begin(), polygon.points().end(), [&](Vector<2> const& a, Vector<2> const& b) {
		return dot(a, direction) < dot(b,direction);
	});
}

ConvexPolygon minkowski_difference(ConvexPolygon const& polygon1, ConvexPolygon const& polygon2) {
	std::vector<Vector<2> > difference;
	for(Vector<2> const& point1 : polygon1.points() )
		for(Vector<2> const& point2 : polygon2.points() )
			difference.push_back(point1 - point2); 
	return ConvexPolygon(std::move(difference) );
}


} //End namespace geomlib
} //End namespace gjk

#endif // GEOMLIB_ALGORITHMS_GJK_HPP
