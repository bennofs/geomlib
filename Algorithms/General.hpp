///////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief This file contains general geometric algorithms.
/// \author Benno Fünfstück
///////////////////////////////////////////////////////////////////////////////

#ifndef GEOMLIB_ALGORITHMS_GENERAL_HPP
#define GEOMLIB_ALGORITHMS_GENERAL_HPP

#include <initializer_list>
#include <iterator>
#include "../Util.hpp"
#include "../Vector.hpp"

namespace geomlib {

///////////////////////////////////////////////////////////////////////////////
/// \brief Tests whether a set of points is on clockwise order.
/// \param [in] begin Iterator to the start of a sequence containing the points.
/// \param [in] end   Iterator to one past the end of the sequence containing the points.
/// \warning The given sequence must at least contain 3 elements, otherwise the result is undefined.
/// \return True if the points are in clockwise order, false otherwise.
///////////////////////////////////////////////////////////////////////////////
template <class IIter>
inline bool is_clockwise(IIter begin, IIter end) {
	DEBUG_ASSERT(std::distance(begin, end) >= 3, "The sequence must at least contain 3 elements");
	typename std::iterator_traits<IIter>::value_type::value_type sum = 0;
	typename std::iterator_traits<IIter>::value_type start = *begin++;
	typename std::iterator_traits<IIter>::value_type current_edge_p1 = start;
	typename std::iterator_traits<IIter>::value_type current_edge_p2;
	while(begin != end) {
		current_edge_p2 = *begin++;
		sum += (current_edge_p2[0] - current_edge_p1[0]) * (current_edge_p2[1] + current_edge_p1[1]);
		current_edge_p1 = current_edge_p2;
	}
	sum += (start[0] - current_edge_p1[0]) * (start[1] + current_edge_p1[1]);
	return sum <= 0;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Overload for initializer lists
/// \copydoc is_clockwise(IIter begin, IIter end)
///////////////////////////////////////////////////////////////////////////////
template <class ValueType>
bool is_clockwise(std::initializer_list<ValueType> init) {
	return is_clockwise(init.begin(), init.end() );
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Tests on which side of a seperator a given point is.
/// \param normal The normal of the seperator (line, plane, etc)
/// \param pointOnSep A point on the seperating line, plane, etc.
/// \param point      The point.
/// \returns True if the point is on the seperator or on the side of the normal,
///          False otherwise.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t D>
bool same_side(Vector<D> const& normal, Vector<D> const& pointOnSep, Vector<D> const& point) {
	return dot(normal, point - pointOnSep) > 0;
}

template <typename Obj1, typename Obj2>
float minimum_distance(Obj1 const& obj1, Obj2 const& obj2) {
	return length(nearest(obj1, obj2).second - nearest(obj1, obj2).first);
}

} // End of namespace geomlib

#endif //GEOMLIB_SHAPES_ALGORITHMS_HPP