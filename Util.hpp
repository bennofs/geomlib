///////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief This file contains various utility functions and classes.
/// \author Benno Fünfstück
///////////////////////////////////////////////////////////////////////////////

#ifndef ENGINE_MATH_UTIL_HPP
#define ENGINE_MATH_UTIL_HPP

#include <climits>
#include <cmath>
#include <limits>
#include <stdexcept>
#include <type_traits>
#include <array>
#include <algorithm>
#include <initializer_list>
#include <sstream>
#include <iterator>
#include <functional>
#include <boost/integer.hpp>
#include <boost/iterator/iterator_facade.hpp>
#include <boost/range/iterator_range.hpp>

namespace geomlib {

///////////////////////////////////////////////////////////////////////////////
/// \brief Contains all util functions.
///////////////////////////////////////////////////////////////////////////////
namespace util {

struct AssertionFailure : public std::logic_error {
	explicit AssertionFailure(std::string const& what) : std::logic_error(what) {}
};

///////////////////////////////////////////////////////////////////////////////
/// \brief Debug assert helper function.
/// \param [in] condition If false, the message is printed (if in debug mode).
/// \param [in] message The message that is printed when the assertion fails.
/// \param [in] line The line
/// \param [in] file The file
/// \param [in] func The function
/// \param [in] str_cond The condition as string.
///////////////////////////////////////////////////////////////////////////////
inline void DebugAssertHelper(bool condition, std::string const& message, unsigned long long line, std::string const& file,
                              std::string const& func, std::string const& str_cond) {
	if(!condition) {
		std::stringstream ss;
		ss << "Assertion [[" << str_cond << "]] failed: " << message << "\n";
		ss << file << ":" << line << ": In function " << func;
		throw AssertionFailure(ss.str());
	}
}

#ifndef NDEBUG
#define DEBUG_ASSERT(cond, message) \
  geomlib::util::DebugAssertHelper(cond, message, __LINE__ , __FILE__ , __func__, #cond)
#else
#define DEBUG_ASSERT(cond, message)
#endif

namespace detail {
	
	template <typename T1, typename T2>
	struct lower_precision_type : 
		std::conditional<
			std::numeric_limits<T1>::digits < std::numeric_limits<T2>::digits,
			T1,
			T2
		> {};
	
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Compares two floating point values, using a maximum ULP difference test.
/// \details You should never use == to compare floating point values, because
///          there are always rounding errors with floating point values. This
///          function checks if the floating point values are nearly equal,
///          meaning the float distance is lower than a given limit.
/// \param [in] f1 The first floating point value.
/// \param [in] f2 The second floating point value.
/// \return True if the values are nearly equal, false otherwise.
///////////////////////////////////////////////////////////////////////////////
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"
template <typename Float1, typename Float2,
          typename std::enable_if<
			 std::is_floating_point<Float1>::value || std::is_floating_point<Float2>::value,
			 int
		  >::type = 0
		 >
bool isEqual(Float1 f1, Float2 f2) {
	if(std::isinf(f1) || std::isinf(f2) ) return f1 == f2; // We can use == here safely.
	typedef typename detail::lower_precision_type<Float1, Float2>::type common_type;
	if( std::abs(f1 - f2) < std::numeric_limits<common_type>::epsilon() * 10 ) return true; // For denormals
	union {
		typename boost::uint_t<sizeof(common_type) * CHAR_BIT>::least val;
		common_type flt;
	} convert1, convert2;
	convert1.flt = f1;
	convert2.flt = f2;
	return std::abs(convert1.val - convert2.val) < 100;
}
#pragma GCC diagnostic pop

///////////////////////////////////////////////////////////////////////////////
/// \brief Compares two values.
/// \details This function is just a overload, so you can use the isEqual function
///          in templates too, where you don't know if the arguments are floating
///          point values.
/// \param [in] i1 The first value.
/// \param [in] i2 The second value.
/// \return True if the values are equal, false otherwise.
///////////////////////////////////////////////////////////////////////////////
template <typename Val1, typename Val2,
          typename std::enable_if<
			!std::is_floating_point<Val1>::value && !std::is_floating_point<Val2>::value,
			int
		  >::type = 0
		 >
bool isEqual(Val1 v1, Val2 v2) { return v1 == v2; }

///////////////////////////////////////////////////////////////////////////////
/// \brief This template calculates the size of the given variadic argument.
/// \tparam Types The variadic argument.
///////////////////////////////////////////////////////////////////////////////
template <typename ... Types>
struct VariadicSize {
	static const std::size_t value = 0;
};

/// Specialization
template <typename Head, typename ... Types>
struct VariadicSize<Head, Types...> {
	static const std::size_t value = VariadicSize<Types...>::value + 1;
};

///////////////////////////////////////////////////////////////////////////////
/// \brief This template can store a variadic argument and expand it into an
///        template later.
/// \tparam Types The types to store.
///////////////////////////////////////////////////////////////////////////////
template <typename ... Types>
struct TypeList {
	///////////////////////////////////////////////////////////////////////////
	/// \brief Insert the types in the given template.
	/// \tparam Template The template to expand.
	///////////////////////////////////////////////////////////////////////////
	template <template<typename...> class Template>
	struct Expand {
		/// The types expanded in the given template.
		typedef Template<Types...> type;
	};

	/// The size of the type list.
	static const std::size_t size = sizeof...(Types);
	
	/// The typelist itself
	typedef TypeList<Types...> type;
};

///////////////////////////////////////////////////////////////////////////////
/// \brief This class "creates" a "new" type from the given type with the same
///        properties and methods.
/// \details Use this class for strong typedefs (typedefs that "create"
///          a "new" type.
/// \tparam T The type to wrap.
/// \tparam Id An unique id. Strong types with the same ID and the same T
///            are the same types.
/// \tparam BaseConvertible Whether to allow assigment and copy construction
///                         from the wrapped type.
/// \note This class does not work for builtin types (int, float, long, double, ...)
///       Please use BOOST_STRONG_TYPEDEF for them.
///////////////////////////////////////////////////////////////////////////////
template <typename T, typename Id, bool BaseConvertible = true>
struct StrongType : T {

	///////////////////////////////////////////////////////////////////////////
	/// \brief Default constructor.
	///////////////////////////////////////////////////////////////////////////
	StrongType() = default;

	///////////////////////////////////////////////////////////////////////////
	/// \brief Forward construtor taking two or more arguments.
	/// \details This constructor just forwards its arguments to the wrapped type's
	///          constructor.
	/// \param arg1 The first argument.
	/// \param arg2 The second argument.
	/// \param args The rest of the arguments.
	///////////////////////////////////////////////////////////////////////////
	template <typename Arg1, typename Arg2, typename ... Args>
	StrongType(Arg1&& arg1, Arg2&& arg2, Args&&... args) :
		T(std::forward<Arg1>(arg1), std::forward<Arg2>(arg2), std::forward<Args>(args)...) {}

	///////////////////////////////////////////////////////////////////////////
	/// \brief Forward constructor taking one argument.
	/// \details \copydetails StrongType::StrongType(Arg1&& arg1, Arg2&& arg2, Args&&... args)
	/// \param arg The argument.
	///////////////////////////////////////////////////////////////////////////
	template <
		typename Arg1,
		typename std::enable_if<
			!std::is_same<
				typename std::remove_cv<
					typename std::remove_reference<Arg1>::type
				>::type,
				T
			>::value || BaseConvertible,
			int
		>::type = 0
	>
	StrongType(Arg1&& arg) : T(std::forward<Arg1>(arg) ) {}

	///////////////////////////////////////////////////////////////////////////
	/// \brief This constructor just forwards the given initializer_list to the
	///        wrapped type's constructor.
	/// \param [in] init The initializer list.
	///////////////////////////////////////////////////////////////////////////
	template <typename V>
	StrongType(std::initializer_list<V> init) : T(init) {}

	///////////////////////////////////////////////////////////////////////////
	/// \brief Assigment operator.
	/// \param [in] other The object to assign from.
	/// \return Reference to *this.
	///////////////////////////////////////////////////////////////////////////
	template <
		typename Arg,
		typename std::enable_if<
			!std::is_same<
				typename std::remove_cv<
					typename std::remove_reference<Arg>::type
				>::type,
				T
			>::value || BaseConvertible,
			int
		>::type = 0
	>
	StrongType& operator=(Arg&& other) {
		T::operator=(std::forward<Arg>(other) );
		return *this;
	}
};

///////////////////////////////////////////////////////////////////////////////
/// \brief This class is a wrapper for std::array that supports initializer_lists
///        and some other constructors.
/// \tparam T Type of element.
/// \tparam N The size of the arary.
/// \tparam DefaultInit Whether to default initialize the elements of the array or not.
///////////////////////////////////////////////////////////////////////////////
template <typename T, std::size_t N, bool DefaultInit = true>
struct Array : public std::array<typename std::remove_reference<T>::type,N> {

	public:

		///////////////////////////////////////////////////////////////////////
		/// \brief Default constructor.
		///////////////////////////////////////////////////////////////////////
		Array() { if(DefaultInit) this->fill(typename std::remove_reference<T>::type()); }

		///////////////////////////////////////////////////////////////////////
		/// \brief Intialize the array with values from the initializer_list.
		/// \param [in] init The intializer list to read the element values from.
		///////////////////////////////////////////////////////////////////////
		template <typename V>
		Array(std::initializer_list<V> init) {
			assert(std::distance(init.begin(), init.end()) == N && "Initializer list's size invalid");
			std::copy(init.begin(), init.end(), this->begin() );
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Intialize the members with the given values.
		/// \param [in] args The values to initialize the members with.
		///////////////////////////////////////////////////////////////////////
		template <
			typename ... Args,
		    typename Enable = typename std::enable_if<VariadicSize<Args...>::value == N>::type
		>
		explicit Array(Args&&... args){
			set_values(std::forward<Args>(args)...);
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Set the values of all elements at once.
		/// \param [in] args The values of the elements.
		///////////////////////////////////////////////////////////////////////
		template <typename ... Args>
		void set_values(Args const&... args) {
			set_values_impl<0, Args...>(args...);
		}

	private:

		///////////////////////////////////////////////////////////////////////
		/// \brief Set_values implementation.
		/// \param [in] head The first argument.
		/// \param [in] tail The other arguments.
		//////////////////////////////////////////////////////////////////////
		template<std::size_t idx, typename Head, typename ... Args>
		void set_values_impl(Head const& head, Args const&... tail) {
			static_assert(idx < N, "Too many arguments to set_values.");
			(*this)[idx] = head;
			set_values_impl<idx+1,Args...>(tail...);
		}

		///////////////////////////////////////////////////////////////////////
		/// \brief Set_values implementation.
		/// \param [in] head The first argument.
		///////////////////////////////////////////////////////////////////////
		template <std::size_t idx, typename Head>
		void set_values_impl(Head const& head) {
			(*this)[idx] = head;
		}
};

//////////////////////////////////////////////////////////////////////////////
/// \brief Clamps a value to a given range.
/// \param [in] val The value to clamp.
/// \param [in] low The lower bound for the value.
/// \param [in] up  The upper bound for the value.
/// \return The value clamped to the range [low, up];
//////////////////////////////////////////////////////////////////////////////
inline float clamp(float val, float low, float up) {
	return std::max(std::min(up, val), low);
}

namespace detail {
	
	template <typename ResultType, bool cache = !std::is_lvalue_reference<ResultType>::value>
	class ReferenceCacheHelper {
		public:	
			
			typedef typename std::remove_reference<ResultType>::type value_type;
			
			ResultType get_reference(ResultType ref) const {
				return ref;
			}
	};
	
	template <typename ResultType>
	class ReferenceCacheHelper<ResultType, true> {
		
		public:
			
			typedef ResultType const value_type;
		
			template <bool Late = false>
			ResultType const& get_reference(ResultType val) const {
				m_cache = val;
				return m_cache;
			}
			
		private:
			
			mutable ResultType m_cache;
	};
	
}

///////////////////////////////////////////////////////////////////////////////
/// \brief An iterator that takes a function and calls that function to
///        get new values. It passes the current index as argument.
/// \details The function is only called once for each incrementation. The iterator
///          is lazy, meaning that the function isn't called if the value isn't needed
///          , i.e. when the iterator isn't dereferenced.
///////////////////////////////////////////////////////////////////////////////
template <typename Function>
class ForIterator : 
public boost::iterator_facade<
	ForIterator<Function>, 
	typename std::remove_reference<typename std::result_of<Function(std::size_t)>::type>::type, 
	std::random_access_iterator_tag,
	typename std::remove_cv<typename std::result_of<Function(std::size_t)>::type>::type
> {
	typedef typename boost::iterator_facade<
		ForIterator<Function>, 
		typename std::remove_reference<typename std::result_of<Function(std::size_t)>::type>::type, 
		std::random_access_iterator_tag,
		typename std::remove_cv<typename std::result_of<Function(std::size_t)>::type>::type
	>::iterator_facade_ iterator_facade_;
	public:

		template <typename Owner, typename std::enable_if<!std::is_pointer<Owner>::value, int>::type = 0>
		ForIterator(Function const& function, std::size_t start_index, Owner const& owner) : 
			m_function(function), m_index(start_index), m_owner(static_cast<void const*>(&owner)) {}
	
		template <typename CompatibleFunction>
		ForIterator(ForIterator<CompatibleFunction> const& other) : m_function(other.m_function), m_index(other.m_index), m_owner(other.m_owner) {}
		
	private:
		
		typename iterator_facade_::reference dereference() const {
			return m_function(m_index);
		}
		
		void advance(typename iterator_facade_::difference_type diff) {
			m_index += diff;
		}
		void increment() { advance(1); }
		void decrement() { advance(-1); }
		
		typename iterator_facade_::difference_type distance_to(ForIterator const& other) const {
			return other.m_index - m_index;
		}
		
		bool equal(ForIterator const& other) const {
			return other.m_owner == m_owner && other.m_index == m_index;
		}
		
		
		Function m_function;
		std::size_t m_index;
		void const* m_owner;
		friend class boost::iterator_core_access;
		template <typename Function2> friend class ForIterator;
};

template <typename Function, typename Owner>
ForIterator<Function> make_for_iterator(Function const& function, std::size_t start_index, Owner const& owner) {
	return ForIterator<Function>(function, start_index, owner);
}

template <typename Function, typename Container>
boost::iterator_range<ForIterator<Function> > make_for_iterator_range(Function const& function, Container const& container) {
	return boost::make_iterator_range(
		ForIterator<Function>(function, 0, container),
		ForIterator<Function>(function, container.size(), container)
	);
}

} //End of namespace util
} //End of namespace geomlib

#endif /* ENGINE_MATH_UTIL_HPP */
