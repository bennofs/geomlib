///////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief Classes and functions for manipulating vectors.
/// \author Benno Fünfstück.
///////////////////////////////////////////////////////////////////////////////

#ifndef GEOMLIB_VECTOR_HPP
#define GEOMLIB_VECTOR_HPP

#include <cstddef>
#include <cassert>
#include <cmath>
#include <array>
#include <algorithm>
#include <functional>
#include <numeric>
#include <type_traits>
#include <initializer_list>
#include <iterator>
#include <ostream>
#include "Util.hpp"

namespace geomlib {

namespace detail_ {
	struct Vector_Type_ID_{};
}

template <std::size_t N, typename T=float>
using Vector = util::StrongType<util::Array<T, N>, detail_::Vector_Type_ID_>;

///////////////////////////////////////////////////////////////////////////////
/// \brief Returns a point at infinity (all components of the vector are infinity.)
/// \return Point at infinity in the given dimension.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T = float>
geomlib::Vector<N,T> PointAtInfinity() {
	geomlib::Vector<N, T> ret;
	ret.fill(std::numeric_limits<T>::infinity() );
	return ret;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Returns the origin.
/// \return A vector that has all components set to 0.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T = float>
geomlib::Vector<N, T> NullVector() {
	geomlib::Vector<N, T> ret;
	ret.fill(0);
	return ret;
}

} // End of namespace geomlib 

///////////////////////////////////////////////////////////////////////////////
/// \brief Computes the dot product of two vectors.
/// \param [in] vec1 The first vector.
/// \param [in] vec2 The second vector.
/// \return The dot product of the two vectors.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T1, typename T2>
typename std::common_type<T1,T2>::type dot(geomlib::Vector<N,T1> const& vec1, geomlib::Vector<N,T2> const& vec2) {
	return std::inner_product(
		vec1.begin(),
		vec1.end(),
		vec2.begin(),
		typename std::common_type<T1,T2>::type()
	);
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Computes the cross product of two 3D vectors.
/// \param [in] vec1 The first vector.
/// \param [in] vec2 The second vector
/// \return The cross product of the two vectors.
///////////////////////////////////////////////////////////////////////////////
template <typename T1, typename T2>
geomlib::Vector<3, typename std::common_type<T1, T2>::type> cross(geomlib::Vector<3,T1> const& vec1, geomlib::Vector<3,T2> const& vec2) {
	return {
		vec1[1]*vec2[2] - vec1[2]*vec2[1],
		vec1[2]*vec2[0] - vec1[0]*vec2[2],
		vec1[0]*vec2[1] - vec1[1]*vec2[0]
	};
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Adds one vector to another vector component-wise.
/// \param [in,out] vec1 The vector to which the other vector should be added.
/// \param [in]     vec2 The vector that should be added to the other vector.
/// \return Reference to vec1.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T1, typename T2>
geomlib::Vector<N,T1>& operator+=(geomlib::Vector<N,T1>& vec1, geomlib::Vector<N,T2> const& vec2) {
	std::transform(
		vec2.begin(),
		vec2.end(),
		vec1.begin(),
		vec1.begin(),
		std::plus<T1>()
	);
	return vec1;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Subtracts a vector from another vector component-wise.
/// \param [in,out] vec1 The vector to subtract from.
/// \param [in]     vec2 The vector to subtract.
/// \return Reference to vec1.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T1, typename T2>
geomlib::Vector<N,T1>& operator-=(geomlib::Vector<N,T1>& vec1, geomlib::Vector<N,T2> const& vec2) {
	std::transform(
		vec1.begin(),
		vec1.end(),
		vec2.begin(),
		vec1.begin(),
		std::minus<T1>()
	);
	return vec1;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Multiplies each component of a vector with the given value.
/// \param [in,out] vec The vector to multiply.
/// \param [in]     val The value to multiply with.
/// \return Reference to vec.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T, typename T2,
	typename Enable = typename std::enable_if<
		std::is_same<
			typename std::common_type<T, T2>::type,
			T
		>::value
	>::type
         >
geomlib::Vector<N,T>& operator*=(geomlib::Vector<N,T>& vec, T2 const& val) {
	std::transform(
		vec.begin(),
		vec.end(),
		vec.begin(),
		std::bind(std::multiplies<T>(), std::placeholders::_1, val)
	);
	return vec;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Divides each component of a vector by the given value.
/// \param [in,out] vec The vector to divide.
/// \param [in]     val The value to divide by.
/// \return Reference to vec.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T, typename T2,
	typename Enable = typename std::enable_if<
		std::is_same<
			typename std::common_type<T, T2>::type,
			T
		>::value
	>::type
         >
geomlib::Vector<N,T>& operator/=(geomlib::Vector<N,T>& vec, T2 const& val) {
	std::transform(
		vec.begin(),
		vec.end(),
		vec.begin(),
		std::bind(std::divides<T>(), std::placeholders::_1, val)
	);
	return vec;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Negates a vector component-wise.
/// \param [in] vec The vector to negate.
/// \return Vector with the negated components of vec.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T>
geomlib::Vector<N,T> operator-(geomlib::Vector<N,T> const& vec) {
	geomlib::Vector<N,T> temp;
	std::transform(
		vec.begin(),
		vec.end(),
		temp.begin(),
		std::negate<T>()
	);
	return temp;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Overload the unary + operator.
/// \param [in] vec The vector.
/// \return The vector vec.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T>
geomlib::Vector<N,T> operator+(geomlib::Vector<N,T> const& vec) {
	return vec;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Adds two vectors component-wise.
/// \param [in] vec1 The first vector.
/// \param [in] vec2 The second vector.
/// \return The result of the addition.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T1, typename T2>
geomlib::Vector<N, typename std::common_type<T1, T2>::type> operator+(geomlib::Vector<N,T1> const& vec1, geomlib::Vector<N,T2> const& vec2) {
	geomlib::Vector<N, typename std::common_type<T1, T2>::type> result;
	std::transform(
		vec1.begin(),
		vec1.end(),
		vec2.begin(),
		result.begin(),
		std::plus<typename std::common_type<T1,T2>::type>()
	);
	return result;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Calculates the difference of two vectors.
/// \param [in] vec1 The first vector.
/// \param [in] vec2 The second vector.
/// \return The difference between the two vectors.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T1, typename T2>
geomlib::Vector<N, typename std::common_type<T1, T2>::type> operator-(geomlib::Vector<N,T1> const& vec1, geomlib::Vector<N,T2> const& vec2) {
	geomlib::Vector<N, typename std::common_type<T1, T2>::type> result;
	std::transform(
		vec1.begin(),
		vec1.end(),
		vec2.begin(),
		result.begin(),
		std::minus<typename std::common_type<T1,T2>::type>()
	);
	return result;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Calculate the product of a vector and a scalar.
/// \param [in] vec The vector.
/// \param [in] val The scalar value.
/// \return The product of the vector and the scalar.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T1, typename T2>
geomlib::Vector<N, typename std::common_type<T1, T2>::type> operator*(geomlib::Vector<N,T1> const& vec, T2 val) {
	geomlib::Vector<N, typename std::common_type<T1, T2>::type> result;
	std::transform(
		vec.begin(),
		vec.end(),
		result.begin(),
		std::bind(
			std::multiplies<typename std::common_type<T1, T2>::type>(),
			std::placeholders::_1,
			val
		)
	);
	return result;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Divides each component of a vector by the given scalar.
/// \param [in] vec The vector.
/// \param [in] val The scalar value to divide with.
/// \return The result of the division.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T1, typename T2>
geomlib::Vector<N, typename std::common_type<T1, T2>::type>
operator/(geomlib::Vector<N,T1> const& vec, T2 val) {
	geomlib::Vector<N, typename std::common_type<T1, T2>::type> result;
	std::transform(
		vec.begin(),
		vec.end(),
		result.begin(),
		std::bind(
			std::divides<typename std::common_type<T1, T2>::type>(),
			std::placeholders::_1,
			val
		)
	);
	return result;
}

///////////////////////////////////////////////////////////////////////////////
/// \copydoc operator*(geomlib::Vector<N,T1> const& vec, T2 val)
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T1, typename T2>
geomlib::Vector<N, typename std::common_type<T1, T2>::type> operator*(T2 val, geomlib::Vector<N,T1> vec) {
	return vec * val;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Compares two vector for equality.
/// \param [in] vec1 The first vector,
/// \param [in] vec2 The second vector.
/// \return True if the vectors are equal (all components are equal), false
///         otherwise.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T1, typename T2>
bool operator==(geomlib::Vector<N,T1> const& vec1, geomlib::Vector<N,T2> const& vec2) {
	return std::equal(
		vec1.begin(),
		vec1.end(),
		vec2.begin(),
		std::bind(
			static_cast<bool(*)(T1, T2)>(
				geomlib::util::isEqual<T1, T2>
			),
			std::placeholders::_1,
			std::placeholders::_2
		)
	);
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Compares two vectors for inequality.
/// \param [in] vec1 The first vector.
/// \param [in] vec2 The second vector.
/// \return False if the vectors are equal, true otherwise.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T1, typename T2>
bool operator!=(geomlib::Vector<N,T1> const& vec1, geomlib::Vector<N,T2> const& vec2) {
	return !(vec1 == vec2);
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Compute the square lenght of the given vector.
/// \param [in] vec The vector to compute the squared lenght of.
/// \return The squared length of the given vector.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T>
T square_length(geomlib::Vector<N,T> const& vec) {
	return dot(vec,vec);
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Compute the length of the given vector.
/// \param [in] vec The vector to compute the length of.
/// \return The length of the given vector.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T>
decltype(std::sqrt(std::declval<T>() ) ) length(geomlib::Vector<N,T> const& vec) {
	return std::sqrt(square_length(vec) );
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Make a normalized vector from the given vector.
/// \param [in] vec The vector which to normalize.
/// \return The given vector, but normalized.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T>
auto normalize(geomlib::Vector<N,T> const& vec)
-> geomlib::Vector<N, decltype(std::declval<T>() / length(vec) )> {
	if(length(vec) == 0) return vec;
	return vec / length(vec);
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Check whether two vectors are collinear.
/// \param [in] vec1 The first vector.
/// \param [in] vec2 The second vector.
/// \return True if the two given vectors are collinear, false otherwise.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T, typename T2>
bool is_collinear(geomlib::Vector<N, T> const& vec1, geomlib::Vector<N, T2> const& vec2) {
	return vec1[0] * vec2 == vec2[0] * vec1;
}

template <typename T, typename T2>
bool is_collinear(geomlib::Vector<1, T> const&, geomlib::Vector<1, T2> const&) {
	return true;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Print a vector to a stream.
/// \param [in,out] out The stream to write to.
/// \param [in] vec The vector to print to the stream.
/// \return The stream out.
///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename T>
std::ostream& operator<<(std::ostream& out, geomlib::Vector<N,T> const& vec) {
	out << "(";
	if(vec.size() == 0) return out << ")";
	std::copy(vec.begin(), vec.end() - 1, std::ostream_iterator<T>(out, "|") );
	return out << *vec.rbegin() << ")";
}

#endif // GEOMLIB_VECTOR_HPP
