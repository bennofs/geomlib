#include <iostream>
#include <type_traits>
#include <clocale>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include "Vector.hpp"
#include "Shapes/Line.hpp"
#include "Shapes/ConvexPolygon.hpp"
#include "Shapes/Triangle.hpp"
#include "Algorithms/General.hpp"
#include "Algorithms/GJK.hpp"

using namespace geomlib;

sf::Vector2f toSFVector2f(Vector<2> const& vec) {
	return {vec[0], vec[1]};
}

Vector<2> toVector(sf::Vector2f const& vec) {
	return {vec.x, vec.y};
}

class Render {
	public:

		Render(sf::RenderTarget& target) : m_target(&target) {}

		void set_target(sf::RenderTarget& target) {
			m_target = &target;
		}

		void operator()(Vector<2> const& p) {
			sf::CircleShape shape(4);
			shape.setPosition(toSFVector2f(p - Vector<2>{2,2}));
			shape.setFillColor(sf::Color::Red);
			m_target->draw(shape);
		}

		void operator()(Line<2> const& l) {
			sf::VertexArray vertices(sf::Lines, 2);
			DEBUG_ASSERT(l.has_min(), "Need min");
			DEBUG_ASSERT(l.has_max(), "Need max");
			vertices[0] = sf::Vertex(toSFVector2f(l.point(l.min() ) ), sf::Color::Yellow);
			vertices[1] = sf::Vertex(toSFVector2f(l.point(l.max() ) ), sf::Color::Yellow);
			m_target->draw(vertices);
		}

		void operator()(Triangle<2> const& t) {
			sf::ConvexShape shape(3);
			for(std::size_t i = 0; i < 3; ++i) shape.setPoint(i, toSFVector2f(t[i]));
			m_target->draw(shape);
		}
		
		void operator()(ConvexPolygon const& p, sf::Color color) {
			sf::ConvexShape shape(p.points().size() );
			shape.setFillColor(color);
			for(int i = 0; i < p.points().size(); ++i) {
				shape.setPoint(i, toSFVector2f(p.points()[i]));
			}
			m_target->draw(shape);
		}

	private:
		sf::RenderTarget* m_target;
};

int main(int argc, char **argv) {
    sf::RenderWindow window(sf::VideoMode(800,600), "GeomLIB Demo");
    Render render(window);
    std::vector<Vector<2> > pts;
    ConvexPolygon p1, p2;
    sf::Clock c;
    unsigned int drawn_frames = 0;
    sf::Text t;
    t.setPosition(500, 10);
    while(window.isOpen() ) {
		for(sf::Event e; window.pollEvent(e); ) {
			switch(e.type) {
				case sf::Event::Closed:
					window.close();
					break;
				case sf::Event::KeyPressed:
					switch(e.key.code) {
						case sf::Keyboard::Escape:
							window.close();
							break;
					}
					break;
				case sf::Event::MouseButtonPressed:
					if(e.mouseButton.button == sf::Mouse::Button::Left) {
						pts.push_back({e.mouseButton.x, e.mouseButton.y});
					}
					break;
			}
		}
		window.clear();
		float secs = c.getElapsedTime().asSeconds();
		if(pts.size() == 4) {
			p1 = ConvexPolygon(pts.begin(), pts.begin() + 4);
		}
		auto mouse_pos = sf::Mouse::getPosition(window);
		if(pts.size() >= 4 && ! has_point(p1, {mouse_pos.x, mouse_pos.y})) render(p1, sf::Color::Blue);
		if(secs >= 0.1) {
			c.restart();
			std::stringstream ss;
			ss << "FPS: " << drawn_frames / secs;
			t.setString(ss.str() );
			drawn_frames = 0;
		}
		window.draw(t);
		window.display();
		drawn_frames++;
    }
    return 0;
}

